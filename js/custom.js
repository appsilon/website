/*------------------------------------------------------------------
Project:    Wolfram
Author:     Yevgeny Simzikov
URL:        http://simpleqode.com/
            https://twitter.com/YevSim
Version:    1.1.1
Created:        18/08/2014
Last change:    19/11/2014
-------------------------------------------------------------------*/

/*
 * Preloader
 */

$(window).load(function(){
    $('.preloader').fadeOut(1000);
});

/*
 * Sidebar
 */

$(".sidebar-btn").on('click', function() {
    $(".sidebar-overlay").toggleClass("show hidden");
    $(".sidebar-menu").toggleClass("show hidden");
    return false;
});

/*
 * Navbar Transparent : Removing and adding on scroll
 */

$('.screen:first').waypoint(function() {
  $(".navbar").toggleClass("navbar-transparent");
}, {
  offset: function() {
    return -$(this).height();
  }
});

/*
 * Backstretch Carousel :: Replace with your images if necessary.
 */

$(".backstretch-carousel").backstretch([
  "img/background_warsaw_blue.jpg",
  "img/background_warsaw_blue.jpg",
  "img/background_plot_blue.jpg",
  "img/background_analysts_blue.jpg",
  "img/background_laptop_blue.jpg",
], {
    duration: 1000,
    fade: 1000
  });

$(".backstretch-carousel").backstretch("pause");

$('.screen').waypoint(function(direction) {
  if (direction === 'down') {
   $(".backstretch-carousel").backstretch("next");
  }
  if (direction === 'up') {
   $(".backstretch-carousel").backstretch("prev");
  }
}, { offset: '50%' });

/*
 * Smooth Scrolling
 */

$(document).ready(function(){
    $('a[href=#welcome],a[href=#how-it-works], a[href=#about], a[href=#hiring], a[href=#contact-us]').bind("click", function(e){
      var anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top
      }, 1000);
      e.preventDefault();
    });
   return false;
});
